package eolcum.invaders;

import org.apache.log4j.Logger;

import eolcum.invaders.input.ShapeInputCLI;
import eolcum.invaders.input.ShapeInvader1;
import eolcum.invaders.match.ProcessPatternOutBorders;
import eolcum.invaders.output.MatchOutputCLI;

/**
 * @author eolcum
 *
 */
public class App {

	private static Logger log; // log4j Logger
	
	public static void main(String[] args) {
		try {
			// Start and configure log4j
			org.apache.log4j.BasicConfigurator.configure();
			log = Logger.getLogger(App.class.getName());
			log.info("Space Invaders started...");
		
			log.info("searching for invaders...");
			SpaceInvaderMatcher sim = new SpaceInvaderMatcher(
					new ShapeInputCLI(), new ShapeInvader1(), new MatchOutputCLI(), new ProcessPatternOutBorders());
			sim.findPatterns(0.75);
		
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}
}