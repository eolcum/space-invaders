package eolcum.invaders.input;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import eolcum.invaders.exception.InvalidImageException;
import eolcum.invaders.model.Image;

/**
 * @author eolcum
 *
 */
public class ShapeInvader2 implements ShapeInput {

	private static Logger log = Logger.getLogger(ShapeInvader2.class.getName());
	/**
	 *	Embedded Invader 2
	 */
	public Image getShape() throws InvalidImageException {
		//Create the image of the first alien pattern
		//---oo---
		//--oooo--
		//-oooooo-
		//oo-oo-oo
		//oooooooo
		//--o--o--
		//-o-oo-o-
		//o-o--o-o
		log.debug("Creating ShapeInvader2...");
		@SuppressWarnings("unchecked")
		List<List<Boolean>> map = Arrays.asList(
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE),
				Arrays.asList(Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE),
				Arrays.asList(Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE),
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE),
				Arrays.asList(Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE)
				);
		return new Image(map);
	}
}
