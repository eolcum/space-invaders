package eolcum.invaders.input;

import eolcum.invaders.exception.InvalidImageException;
import eolcum.invaders.model.Image;

public interface ShapeInput {
	Image getShape() throws InvalidImageException;
}
