package eolcum.invaders.input;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import eolcum.invaders.exception.InvalidImageException;
import eolcum.invaders.model.Image;

/**
 * @author eolcum
 *
 */
public class ShapeInvader1 implements ShapeInput {

	private static Logger log = Logger.getLogger(ShapeInvader1.class.getName());
	/**
	 *	Embedded Invader 1
	 */
	public Image getShape() throws InvalidImageException {
		//Create the image of the first alien pattern
		//--o-----o--
		//---o---o---
		//--ooooooo--
		//-oo-ooo-oo-
		//ooooooooooo
		//o-ooooooo-o
		//o-o-----o-o
		//---oo-oo---
		log.debug("Creating ShapeInvader1...");
		@SuppressWarnings("unchecked")
		List<List<Boolean>> map = Arrays.asList(
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE),
				Arrays.asList(Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE),
				Arrays.asList(Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE),
				Arrays.asList(Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE),
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE)
				);
		return new Image(map);
	}
}
