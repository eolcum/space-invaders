package eolcum.invaders.input;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import eolcum.invaders.exception.InvalidImageException;
import eolcum.invaders.model.Image;

public class ShapeInputCLI implements ShapeInput {

	private final static Logger log = Logger.getLogger(ShapeInputCLI.class.getName());

	public Image getShape() throws InvalidImageException {
		log.debug("getShape started.");
		String input = getStringFromCLI(); // Get the command line input as a single line String
		return getImage(input); // Transform the string to an Image object

	}

	/**
	 * @return The CLI input as a single String object
	 */
	private String getStringFromCLI() {
		log.debug("Getting the map from the command line...");
		Scanner scanner = new Scanner(System.in); // Create a Scanner object
		System.out.println("Give me the radar input:");

		StringBuilder radarInput = new StringBuilder();
		String line;
		do {
			line = scanner.nextLine();
			radarInput.append(line); // Append every line
			radarInput.append("/n"); // Append /n for end of line
		} while (scanner.hasNext() && !scanner.hasNext("~~~~")); // Stop when "~~~~" comes
		if(scanner.hasNext("~~~~")) {
			radarInput.append(scanner.nextLine());
		}
		scanner.close();
		log.trace("Command Line scanner closed.");
		return radarInput.toString();
	}

	/**
	 * @param cliString to be transformed in an Image
	 * @return the Image object
	 * @throws Exception If the Image can not be initialized.
	 */
	private Image getImage(String cliString) throws InvalidImageException {
		log.debug("Generating image from cliString: " + cliString);
		cliString = cliString.replace("~", ""); // Remove start and end tags
		ArrayList<List<Boolean>> map = new ArrayList<List<Boolean>>();
		// Transfer the map string to char array and create two dimensional list.
		char[] inputArr = cliString.toCharArray();

		List<Boolean> row = new ArrayList<Boolean>();

		// Iterate of inputArr create the row arrays when '/' new line arrives
		for (int i = 0; i < inputArr.length; i++) {

			// If new line has come '/'
			if (inputArr[i] == '/') {
				i++; // pass the character after '/'

				// If the row is not empty, add to rows
				if (row.size() > 0) {
					map.add(row);
					row = new ArrayList<Boolean>();
				}
			} else {
				// For every char put a boolean value,
				// Boolean has better performance in comparison for large maps
				Boolean node = null;
				if ('o' == inputArr[i]) {
					node = Boolean.TRUE;
				} else if ('-' == inputArr[i]) {
					node = Boolean.FALSE;
				}
				row.add(node);
			}
		}
		return new Image(map);
	}
}
