package eolcum.invaders.exception;

public class InvalidImageException extends Exception {
    public InvalidImageException(String message) 
    { 
        super(message); 
    } 
}
