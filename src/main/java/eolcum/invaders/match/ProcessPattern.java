package eolcum.invaders.match;

import java.util.List;

import eolcum.invaders.model.Image;
import eolcum.invaders.model.Match;

public interface ProcessPattern {
	/**
	 * @param map			The map to be searched		
	 * @param pattern		The other image pattern to recognize
	 * @param minProximity	Value between [0-1], minimum match rate
	 * 						to recognize a comparison as a match
	 * @return				The list of pattern matches higher than 
	 * 						passed proximity rate
	 */
	List<Match> findMatches(Image map, Image pattern, double minProximity);
}
