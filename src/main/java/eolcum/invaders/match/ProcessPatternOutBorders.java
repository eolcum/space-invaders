package eolcum.invaders.match;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import eolcum.invaders.model.Image;
import eolcum.invaders.model.Match;

public class ProcessPatternOutBorders implements ProcessPattern{

	private static Logger log = Logger.getLogger(ProcessPatternOutBorders.class.getName());

	public List<Match> findMatches(Image map, Image pattern, double minProximity) {
		log.debug("findMatches started with minProximity: " + minProximity);
		List<Match> matchList = new ArrayList<Match>(); //Result list to be returned.
		Match match;
		
		//start from the (-) values to find the partial matches.
		int startingY = 1 + (pattern.getHeight() * -1);
		int startingX = 1 + (pattern.getWidth() * -1);
		
		//Check every point as the starting point on the map 
		for (int i = startingX; i < map.getWidth(); i++) {
			for (int j = startingY; j < map.getHeight(); j++) {
				match = compareStartingFrom(map, pattern, i, j);
				if (match.getProximityRate() >= minProximity)
					matchList.add(match);
			}
		}
		log.debug("findMatches finished found " + matchList.size() + " matches.");
		return matchList;
	}
	
	/**
	 * @param compared	The other image pattern to recognize
	 * @param iX		axis index of the main map to start checking this comparison
	 * 					iteration
	 * @param iY		ordinate index of the main map to start checking this comparison
	 * 					iteration
	 * @return
	 */
	private Match compareStartingFrom(Image map, Image compared, int iX, int iY) {
		log.debug("compareOneToOne started with iX: " + iX + " iY: " + iY);
		int matchedPoint = 0;
		
		//Start from 0 to height and width of the pattern to find the proximity rate.
		for (int i = 0; i < compared.getWidth(); i++) {
			for (int j = 0; j < compared.getHeight(); j++) {
				//If the point to be checked is out of the map take it as a non-matched dot.
				if (iX + i < 0 || iY + j < 0 || iX + i >= map.getWidth() || iY + j >= map.getHeight()) {
					continue;
				} else if (map.getMap().get(iY + j).get(iX + i).equals(compared.getMap().get(j).get(i))) {
					//else if the dot values the same, it is a match
					matchedPoint++;
				}
			}
		}
		
		//Proximity (Match) rate is the ratio of matched dots to all dots.
		double proximityRate = ((double) matchedPoint) / (compared.getHeight() * compared.getWidth());
		Match match = new Match(compared.getWidth(), compared.getHeight(), iX, iY, proximityRate);
		return match;
	}
}
