package eolcum.invaders;

import java.util.List;

import org.apache.log4j.Logger;

import eolcum.invaders.exception.InvalidImageException;
import eolcum.invaders.input.ShapeInput;
import eolcum.invaders.match.ProcessPattern;
import eolcum.invaders.model.Image;
import eolcum.invaders.model.Match;
import eolcum.invaders.output.MatchOutput;

public class SpaceInvaderMatcher {
	private final static Logger log = Logger.getLogger(SpaceInvaderMatcher.class.getName());
	
	private ShapeInput mapInput;
	private ShapeInput invaderInput;
	private MatchOutput matchOutput;
	private ProcessPattern processPattern;

	public SpaceInvaderMatcher() {}

	public SpaceInvaderMatcher(ShapeInput mapInput, ShapeInput invaderInput, MatchOutput matchOutput, ProcessPattern processPattern) {
		this.matchOutput = matchOutput;
		this.mapInput = mapInput;
		this.invaderInput = invaderInput;
		this.processPattern = processPattern;
	}

	public List<Match> findPatterns(double proximityRate) throws InvalidImageException {

		log.info("findPatterns started...");
		
		Image map = this.mapInput.getShape();
		Image invader = this.invaderInput.getShape();

		List<Match> matches = this.processPattern.findMatches(map, invader, proximityRate);
		
		this.matchOutput.output(matches);
		
		return matches;
		    
	}

	public ShapeInput getMapInput() {
		return mapInput;
	}

	public void setMapInput(ShapeInput mapInput) {
		this.mapInput = mapInput;
	}

	public ShapeInput getInvaderInput() {
		return invaderInput;
	}

	public void setInvaderInput(ShapeInput invaderInput) {
		this.invaderInput = invaderInput;
	}

	public MatchOutput getMatchOutput() {
		return matchOutput;
	}

	public void setMatchOutput(MatchOutput matchOutput) {
		this.matchOutput = matchOutput;
	}

	public ProcessPattern getProcessPattern() {
		return processPattern;
	}

	public void setProcessPattern(ProcessPattern processPattern) {
		this.processPattern = processPattern;
	}
	
}
