package eolcum.invaders.output;

import java.util.List;

import org.apache.log4j.Logger;

import eolcum.invaders.model.Match;

public class MatchOutputCLI implements MatchOutput {

	private static Logger log = Logger.getLogger(MatchOutputCLI.class.getName());
	
	public void output(List<Match> matchList) {
		log.debug("MatchOutputCLI.output started.");
		for (Match match : matchList) {
			System.out.println(match.toString());
		}
	}
}
