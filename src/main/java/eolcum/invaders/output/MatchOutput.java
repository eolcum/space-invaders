package eolcum.invaders.output;

import java.util.List;

import eolcum.invaders.model.Match;

public interface MatchOutput {
	void output(List<Match> matchList);
}
