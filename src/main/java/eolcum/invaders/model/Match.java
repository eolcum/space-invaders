package eolcum.invaders.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author eolcum
 *
 */
public class Match {
	private int width; // width of the pattern
	private int height; // height of the pattern

	private int axis; // starting axis of the matching pattern starts in the main map
	private int ordinate; // starting ordinate of the matching pattern starts in the main map

	private double proximityRate; // proximity rate of the pattern

	public Match(int width, int height, int axis, int ordinate, double proximityRate) {
		this.width = width;
		this.height = height;
		this.axis = axis;
		this.ordinate = ordinate;
		this.proximityRate = proximityRate;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getAxis() {
		return axis;
	}

	public void setAxis(int axis) {
		this.axis = axis;
	}

	public int getOrdinate() {
		return ordinate;
	}

	public void setOrdinate(int ordinate) {
		this.ordinate = ordinate;
	}

	public double getProximityRate() {
		return proximityRate;
	}

	public void setProximityRate(double proximity) {
		this.proximityRate = proximity;
	}

	public String toString() {
		return "{ Match Rate: " + new BigDecimal(proximityRate * 100).setScale(0, RoundingMode.HALF_DOWN) + "% | Axis: "
				+ axis + " | Ordinate: " + ordinate + " | Width " + width + " | Height: " + height + " }";

	}

}
