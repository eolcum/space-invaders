package eolcum.invaders.model;

import java.util.List;

import org.apache.log4j.Logger;

import eolcum.invaders.exception.InvalidImageException;

/**
 * @author eolcum
 *
 */
public class Image {

	private List<List<Boolean>> map;
	private int width;
	private int height;

	static Logger log = Logger.getLogger(Image.class.getName());

	/**
	 * @param input			Input String to be transformed into a map
	 * @throws Exception	throws Exception if the input is invalid
	 */
	public Image(List<List<Boolean>> map) throws InvalidImageException {
		this.map = map;
		validate();
		//Set dimensions of the image
		this.height = map.size();
		this.width = map.get(0).size();
	}
	
	/**
	 * @throws Exception	if the image is invalid.
	 */
	private void validate() throws InvalidImageException {
		int width = -1;//to check if the size of every row is the same
		if(map.size() == 0) {
			throw new InvalidImageException("Map can not be empty");
		}
		for (List<Boolean> list : map) {
			if(width == -1)  {
				width = list.size(); //Initialize width if necessary
			} else if(list.size() != width) {
				throw new InvalidImageException("Every row should be the same length");
			}
			for (Boolean bool : list) {
				if(bool == null) {
					throw new InvalidImageException("Invalid character in the map, only 'o' and '-' is accepted.");
				}
			}
		}
	}

	public List<List<Boolean>> getMap() {
		return map;
	}


	public void setMap(List<List<Boolean>> map) {
		this.map = map;
	}


	public int getWidth() {
		return width;
	}


	public void setWidth(int width) {
		this.width = width;
	}


	public int getHeight() {
		return height;
	}


	public void setHeight(int height) {
		this.height = height;
	}


	public static void setLog(Logger log) {
		Image.log = log;
	}


}
