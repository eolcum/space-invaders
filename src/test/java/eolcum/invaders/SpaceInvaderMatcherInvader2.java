package eolcum.invaders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.junit.Test;
import org.junit.Before;
import eolcum.invaders.exception.InvalidImageException;
import eolcum.invaders.input.ShapeInputCLI;
import eolcum.invaders.input.ShapeInvader2;
import eolcum.invaders.match.ProcessPatternOutBorders;
import eolcum.invaders.model.Match;
import eolcum.invaders.output.MatchOutputCLI;

/**
 * Unit test for SpaceInvaderMatcher.
 */
public class SpaceInvaderMatcherInvader2 {
	private String map;

	@Before
	public void before() {
		map = "~~~~\n"
				+ "----o--oo----o--ooo--ooo---------o---oo-o----oo---o--o---------o----o------o----------------o--o--o-\n"
				+ "--o-o-----oooooooo-oooooo-------o----o------ooo-o---o--o----o------o--o---ooo-----o--oo-o------o----\n"
				+ "--o--------oo-ooo-oo-oo-oo------------------ooooo-----o-----o------o---o--o--o-o-o------o----o-o-o--\n"
				+ "-------o--oooooo--o-oo-o--o-o-----oo--o-o-oo--o-oo-oo-o--------o-----o------o-ooooo---o--o--o-------\n"
				+ "------o---o-ooo-ooo----o------o-------o---oo-ooooo-o------o----o--------o-oo--ooo-oo-------------o-o\n"
				+ "-o--o-----o-o---o-ooooo-o-------o----o---------o-----o-oo-----------oo----ooooooo-ooo-oo------------\n"
				+ "o-------------ooooo-o--o--o--o-------o--o-oo-oo-o-o-o----o-------------o--oooo--ooo-o----o-----o--o-\n"
				+ "--o-------------------------oo---------oo-o-o--ooo----o-----o--o--o----o--o-o-----o-o------o-o------\n"
				+ "-------------------o-----------------o--o---------------o--------o--oo-o-----oo-oo---o--o---o-----oo\n"
				+ "----------o----------o------------------o--o----o--o-o------------oo------o--o-o---o-----o----------\n"
				+ "------o----o-o---o-----o-o---------oo-o--------o---------------------------------o-o-o--o-----------\n"
				+ "---------------o-------------o-------o-------------------o-----o---------o-o-------------o-------oo-\n"
				+ "-o--o-------------o-o--------o--o--oo-------------o----ooo----o-------------o----------oo----o---o-o\n"
				+ "-o--o-------------o----oo------o--o-------o--o-----------o----o-----o--o----o--oo-----------o-------\n"
				+ "-o-----oo-------o------o---------------o--o----------o-----o-------o-----------o---o-o--oooooo-----o\n"
				+ "-o--------o-----o-----o---------oo----oo---o-----------o---o--oooo-oo--o-------o------oo--oo--o-----\n"
				+ "------------o-------------------o----oooo-------------oo-oo-----ooo-oo-----o-------o-oo-oooooooo---o\n"
				+ "-----------------------------------oooooooo---o-----o-------o--oooooo-o------------o-o-ooooooo-o----\n"
				+ "------------o------o-------o-------oo-oo--o--o---------o--o-o-o-ooooo-o--------------oo-o----o-oo-o-\n"
				+ "---o-o----------o--------oo----o----oooooooo-------o----o-o-o-o-----o-o-----o----------ooo-oo--o---o\n"
				+ "-o-o---------o-o---------------o--o--o--ooo---ooo-------o------oo-oo------------o--------o--o-o--o--\n"
				+ "-------oo---------------------------o-oo----------o------o-o-------o-----o----o-----o-oo-o-----o---o\n"
				+ "---o--------o-----o-------o-oo-----oo--oo-o----oo----------o--o---oo------oo----o-----o-------o-----\n"
				+ "---o--ooo-o---------o-o----o------------o---------o----o--o-------o-------------o----------------oo-\n"
				+ "---o------o----------------o----o------o------o---oo-----------o-------------o----------oo---------o\n"
				+ "--oo---------------o--o------o---o-----o--o-------------o------o-------o-----o-----o----o------o--o-\n"
				+ "-o-------o----------o-o-o-------o-----o--o-o-----------o-oo-----------o------o---------o-----o-o----\n"
				+ "----------o----o-------o----o--o------o------------o---o---------------oo----o-----ooo--------------\n"
				+ "----o--------oo----o-o----o--o------ooo----o-oooo---o--o-oo--------o-oo-----o-o---o-o--o-----oo-----\n"
				+ "------o--------o-ooooo----o---o--o-----o---------------o-o-------o-----o----------------------------\n"
				+ "o-------oo----o--oooooo-o---o--o------oooo----------o-oo-------o---o----------o------oo-------------\n"
				+ "-o---o----------o--oo-oo-o---o-----o-o-----------------------oo--o------o------o--------------------\n"
				+ "-----oo-o-o-o---ooooooooo----o----o--------o--o---oo---o------------o----------o-o---o------o-o--oo-\n"
				+ "------o------o---ooo-o---------------------------o--o---o---o----o--o-------o-----o------o----o----o\n"
				+ "-------o----------ooo-o-----o----o---o--o-oo--o--o-o--o------o--o-oo---ooo------------------------o-\n"
				+ "-o-------o------o-o--ooo--o---o---oo-----o----o-------------o----o-ooo-o------o--o-o------o-o-------\n"
				+ "---oo--o---o-o---------o---o--------------o--o-----o-------o-----o--o---o-oo--------o----o----o-----\n"
				+ "o------o----oo-o-----------oo--o---o--------o-o------o-------o-o------o-oo---------o-----oo---------\n"
				+ "----o--o---o-o-----------o---o------------o-------o----o--o--o--o-o---------------o-----------------\n"
				+ "-------oo--o-o-----o-----o----o-o--o----------------------o-------o------o----oo----ooo---------o---\n"
				+ "o-----oo-------------------o--o-----o-----------o------o-------o----o-----------o----------------o--\n"
				+ "--o---o-------o------------o--------------------o----o--o-------------oo---o---------oo--------o----\n"
				+ "--o--------o---------o------------o------o-------o------------o-------o---o---------ooooo-----------\n"
				+ "------o--------------o-o-o---------o---o-------o--o-----o-------o-o----------o-----oo-ooo----------o\n"
				+ "--o---------------o----o--oo-------------o---------o-------------------oo---------oo-o-ooo----------\n"
				+ "-o-----------o------ooo----o----------------ooo-----o--------o--o---o-----------o-o-oooooo--------oo\n"
				+ "-o---o-------o---o-oooo-----o-------------------o----oo-----------------o--o--------o--o------o--o--\n"
				+ "-------o---o------oooooo--o----ooo--o--------o-------o----------------------------oo-oo-o--o--------\n"
				+ "o--oo------o-----oo--o-oo------------oo--o------o--o-------------oo----o------------oooo-o------oo--\n"
				+ "-----o----------ooooooooo--------------oo--------------oo-----o-----o-o--o------o----------o----o---\n"
				+ "~~~~";
		InputStream in = new ByteArrayInputStream(this.map.getBytes());
		System.setIn(in);
	}

	@Test
	public void findInvader1() throws InvalidImageException {
		SpaceInvaderMatcher spaceInvaderMatcher = new SpaceInvaderMatcher(new ShapeInputCLI(), new ShapeInvader2(),
				new MatchOutputCLI(), new ProcessPatternOutBorders());
		List<Match> matches = spaceInvaderMatcher.findPatterns(0.87);
		assertNotNull(matches);
		assertEquals("should have three matches", matches.size(), 1);
		assertEquals("should have three matches", matches.get(0).getAxis(), 42);
		assertEquals("should have three matches", matches.get(0).getOrdinate(), 0);
	}
}
