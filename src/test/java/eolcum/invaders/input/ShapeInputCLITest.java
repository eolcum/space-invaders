package eolcum.invaders.input;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import eolcum.invaders.exception.InvalidImageException;

public class ShapeInputCLITest {

	private String invalidMapString;
	private ShapeInput shapeInput;

	@Before
	public void initialize() {
		this.shapeInput = new ShapeInputCLI();
	}
	
	@Test(expected = InvalidImageException.class)
	public void testInvalidWidth() throws InvalidImageException {
		this.invalidMapString = "~~~~\n"
				+ "----o--oo----o--ooo--ooo---------o---oo-o-------o--o--o-\n"
				+ "--o-o-----oooooooo-oooooo--o------o--o---ooo-----o--oo-o------o--\n"
				+ "--o--------oo-ooo-oo-oo-oo-------o-o-o------o----o-o-o--\n"
				+ "~~~~\n";
		InputStream in = new ByteArrayInputStream(this.invalidMapString.getBytes());
		System.setIn(in);
		this.shapeInput.getShape();
	}
	
	@Test(expected = InvalidImageException.class)
	public void testZeroWidth() throws InvalidImageException {
		this.invalidMapString = "~~~~\n"
				+ "~~~~\n";
		InputStream in = new ByteArrayInputStream(this.invalidMapString.getBytes());
		System.setIn(in);
		this.shapeInput.getShape();
	}
	
	@Test(expected = InvalidImageException.class)
	public void testWithoutFirstTilda() throws InvalidImageException {
		this.invalidMapString = "----o--oo----o--ooo--ooo---------o---oo-o-\n"
				+ "--o-o-----oooooooo-oooooo--o------o--o---ooo-----o--oo-o\n"
				+ "--o--------oo-ooo-oo-oo-oo-------o-o-o------o----o-o-o--\n"
				+ "~~~~\n";
		InputStream in = new ByteArrayInputStream(this.invalidMapString.getBytes());
		System.setIn(in);
		this.shapeInput.getShape();
	}
}