package eolcum.invaders.input;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import eolcum.invaders.exception.InvalidImageException;
import eolcum.invaders.model.Image;

@RunWith(Parameterized.class)
public class ShapeInputTest {

	private ShapeInput shapeInput;
	private String validMapString;
	
	//Parameter is passed to shapeInput
	public ShapeInputTest(ShapeInput shapeInput) {
		this.shapeInput = shapeInput;
		
	}

	@Parameters
	public static Collection<Object[]> getParameters() {
		return Arrays.asList(new Object[][] { { new ShapeInvader1() }, { new ShapeInvader2() }, { new ShapeInputCLI() } });
	}
	
	@Before
	public void initialize() {
		this.validMapString = "~~~~\n"
				+ "----o--oo----o--ooo--ooo---------o---oo-o----oo---o--o---------o----o------o----------------o--o--o-\n"
				+ "--o-o-----oooooooo-oooooo-------o----o------ooo-o---o--o----o------o--o---ooo-----o--oo-o------o----\n"
				+ "--o--------oo-ooo-oo-oo-oo------------------ooooo-----o-----o------o---o--o--o-o-o------o----o-o-o--\n"
				+ "-------o--oooooo--o-oo-o--o-o-----oo--o-o-oo--o-oo-oo-o--------o-----o------o-ooooo---o--o--o-------\n"
				+ "------o---o-ooo-ooo----o------o-------o---oo-ooooo-o------o----o--------o-oo--ooo-oo-------------o-o\n"
				+ "-o--o-----o-o---o-ooooo-o-------o----o---------o-----o-oo-----------oo----ooooooo-ooo-oo------------\n"
				+ "o-------------ooooo-o--o--o--o-------o--o-oo-oo-o-o-o----o-------------o--oooo--ooo-o----o-----o--o-\n"
				+ "--o-------------------------oo---------oo-o-o--ooo----o-----o--o--o----o--o-o-----o-o------o-o------\n"
				+ "-------------------o-----------------o--o---------------o--------o--oo-o-----oo-oo---o--o---o-----oo\n"
				+ "----------o----------o------------------o--o----o--o-o------------oo------o--o-o---o-----o----------\n"
				+ "------o----o-o---o-----o-o---------oo-o--------o---------------------------------o-o-o--o-----------\n"
				+ "---------------o-------------o-------o-------------------o-----o---------o-o-------------o-------oo-\n"
				+ "-o--o-------------o-o--------o--o--oo-------------o----ooo----o-------------o----------oo----o---o-o\n"
				+ "-o--o-------------o----oo------o--o-------o--o-----------o----o-----o--o----o--oo-----------o-------\n"
				+ "-o-----oo-------o------o---------------o--o----------o-----o-------o-----------o---o-o--oooooo-----o\n"
				+ "-o--------o-----o-----o---------oo----oo---o-----------o---o--oooo-oo--o-------o------oo--oo--o-----\n"
				+ "------------o-------------------o----oooo-------------oo-oo-----ooo-oo-----o-------o-oo-oooooooo---o\n"
				+ "-----------------------------------oooooooo---o-----o-------o--oooooo-o------------o-o-ooooooo-o----\n"
				+ "------------o------o-------o-------oo-oo--o--o---------o--o-o-o-ooooo-o--------------oo-o----o-oo-o-\n"
				+ "---o-o----------o--------oo----o----oooooooo-------o----o-o-o-o-----o-o-----o----------ooo-oo--o---o\n"
				+ "-o-o---------o-o---------------o--o--o--ooo---ooo-------o------oo-oo------------o--------o--o-o--o--\n"
				+ "-------oo---------------------------o-oo----------o------o-o-------o-----o----o-----o-oo-o-----o---o\n"
				+ "---o--------o-----o-------o-oo-----oo--oo-o----oo----------o--o---oo------oo----o-----o-------o-----\n"
				+ "---o--ooo-o---------o-o----o------------o---------o----o--o-------o-------------o----------------oo-\n"
				+ "---o------o----------------o----o------o------o---oo-----------o-------------o----------oo---------o\n"
				+ "--oo---------------o--o------o---o-----o--o-------------o------o-------o-----o-----o----o------o--o-\n"
				+ "-o-------o----------o-o-o-------o-----o--o-o-----------o-oo-----------o------o---------o-----o-o----\n"
				+ "----------o----o-------o----o--o------o------------o---o---------------oo----o-----ooo--------------\n"
				+ "----o--------oo----o-o----o--o------ooo----o-oooo---o--o-oo--------o-oo-----o-o---o-o--o-----oo-----\n"
				+ "------o--------o-ooooo----o---o--o-----o---------------o-o-------o-----o----------------------------\n"
				+ "o-------oo----o--oooooo-o---o--o------oooo----------o-oo-------o---o----------o------oo-------------\n"
				+ "-o---o----------o--oo-oo-o---o-----o-o-----------------------oo--o------o------o--------------------\n"
				+ "-----oo-o-o-o---ooooooooo----o----o--------o--o---oo---o------------o----------o-o---o------o-o--oo-\n"
				+ "------o------o---ooo-o---------------------------o--o---o---o----o--o-------o-----o------o----o----o\n"
				+ "-------o----------ooo-o-----o----o---o--o-oo--o--o-o--o------o--o-oo---ooo------------------------o-\n"
				+ "-o-------o------o-o--ooo--o---o---oo-----o----o-------------o----o-ooo-o------o--o-o------o-o-------\n"
				+ "---oo--o---o-o---------o---o--------------o--o-----o-------o-----o--o---o-oo--------o----o----o-----\n"
				+ "o------o----oo-o-----------oo--o---o--------o-o------o-------o-o------o-oo---------o-----oo---------\n"
				+ "----o--o---o-o-----------o---o------------o-------o----o--o--o--o-o---------------o-----------------\n"
				+ "-------oo--o-o-----o-----o----o-o--o----------------------o-------o------o----oo----ooo---------o---\n"
				+ "o-----oo-------------------o--o-----o-----------o------o-------o----o-----------o----------------o--\n"
				+ "--o---o-------o------------o--------------------o----o--o-------------oo---o---------oo--------o----\n"
				+ "--o--------o---------o------------o------o-------o------------o-------o---o---------ooooo-----------\n"
				+ "------o--------------o-o-o---------o---o-------o--o-----o-------o-o----------o-----oo-ooo----------o\n"
				+ "--o---------------o----o--oo-------------o---------o-------------------oo---------oo-o-ooo----------\n"
				+ "-o-----------o------ooo----o----------------ooo-----o--------o--o---o-----------o-o-oooooo--------oo\n"
				+ "-o---o-------o---o-oooo-----o-------------------o----oo-----------------o--o--------o--o------o--o--\n"
				+ "-------o---o------oooooo--o----ooo--o--------o-------o----------------------------oo-oo-o--o--------\n"
				+ "o--oo------o-----oo--o-oo------------oo--o------o--o-------------oo----o------------oooo-o------oo--\n"
				+ "-----o----------ooooooooo--------------oo--------------oo-----o-----o-o--o------o----------o----o---\n"
				+ "~~~~\n";
	}

	@Test
	public void getShapeTest() throws InvalidImageException {
		InputStream in = new ByteArrayInputStream(this.validMapString.getBytes());
		System.setIn(in);
		Image image = shapeInput.getShape();
		assertNotNull(image);
		assertNotNull(image.getMap());
		assertTrue(image.getMap() instanceof List);
		assertTrue(image.getMap().size() > 0);
		assertTrue(image.getMap().get(0) instanceof List);
		assertTrue(image.getMap().get(0).size() > 0);
		assertTrue(image.getMap().get(0).get(0) instanceof Boolean);
		assertEquals(image.getHeight(), image.getMap().size());
		assertEquals(image.getWidth(), image.getMap().get(0).size());
		for (List<Boolean> row: image.getMap()) {
			for (Boolean bool : row) {
				assertNotNull(bool);
			}
		}
	}
}
