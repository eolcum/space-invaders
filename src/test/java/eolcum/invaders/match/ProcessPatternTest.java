package eolcum.invaders.match;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import eolcum.invaders.exception.InvalidImageException;
import eolcum.invaders.model.Image;
import eolcum.invaders.model.Match;

public class ProcessPatternTest {
	private Image image;
	private Image partialImage;
	
	private ProcessPattern processPattern;

	@SuppressWarnings("unchecked")
	@Before
	public void initialize() throws InvalidImageException {
		this.processPattern = new ProcessPatternOutBorders();
		//This image defines invader1
		//--o-----o--
		//---o---o---
		//--ooooooo--
		//-oo-ooo-oo-
		//ooooooooooo
		//o-ooooooo-o
		//o-o-----o-o
		//---oo-oo---
		this.image = new Image(Arrays.asList(
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE),
				Arrays.asList(Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE),
				Arrays.asList(Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE),
				Arrays.asList(Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE),
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE)
				));
		//This image is the same with image but it lacks the last row
		this.partialImage = new Image(Arrays.asList(
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
				Arrays.asList(Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE),
				Arrays.asList(Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE),
				Arrays.asList(Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE),
				Arrays.asList(Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE)
				));
	}
	
	@Test
	public void fullMatch() throws InvalidImageException {
		List<Match> matches = this.processPattern.findMatches(image, image, 1);
		assertEquals("image should be found in a map of itself", matches.size(), 1);
	}
	
	@Test
	public void partialMatch() throws InvalidImageException {
		List<Match> matches = this.processPattern.findMatches(image, partialImage, 1);
		assertEquals("image should be found in a map contains itself",matches.size(), 1);
		
	}
	
	@Test
	public void matchInPartialMap() throws InvalidImageException {
		List<Match> matches = this.processPattern.findMatches(partialImage, image, 0.8);
		assertTrue("image should be found with a lower proximity, in a map which has partial of itself",matches.get(0).getProximityRate() >= 0.86);
		assertTrue("image should be found with a lower proximity, in a map which has partial of itself",matches.get(0).getProximityRate() <= 0.88);
	}
}
